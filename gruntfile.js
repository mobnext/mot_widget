'use strict';

module.exports = function(grunt) {
    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            jade: {
                files: ['views/**'],
                options: {
                    livereload: true,
                },
            },
            js: {
                files: ['public/**/*.js', 'modules/**/*.js', 'config/**/*.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true,
                },
            },
            html: {
                files: ['public/views/**'],
                options: {
                    livereload: true,
                },
            },
            css: {
                files: ['public/css/**'],
                options: {
                    livereload: true
                }
            }
        },
        jshint: {
            all: ['gruntfile.js', 'public/js/**/*.js', 'test/mocha/**/*.js', 'test/karma/**/*.js', 'app/**/*.js'],
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            }
        },
        copy: {
            options: {
                punctuation: ''
            },
            js: {
                files: [
                    { cwd: 'bower_components/jquery', src: ['dist/*.js'], dest: 'public/lib/jquery', expand: true },
                    { cwd: 'bower_components/jquery-ui', src: ['*.js'], dest: 'public/lib/jquery-ui', expand: true },
                    {
                        cwd: 'bower_components/jquery.easing',
                        src: ['js/*.js'],
                        dest: 'public/lib/jquery.easing',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/jquery.easy-pie-chart',
                        src: ['dist/*.js'],
                        dest: 'public/lib/jquery.easy-pie-chart',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/chart.js',
                        src: ['dist/*.js'],
                        dest: 'public/lib/chart.js',
                        expand: true
                    },

                    {
                        cwd: 'bower_components/amcharts-stock',
                        src: ['dist/**/*.js'],
                        dest: 'public/lib/amcharts-stock',
                        expand: true
                    },
                    { cwd: 'bower_components/angular', src: ['*.js'], dest: 'public/lib/angular', expand: true },
                    {
                        cwd: 'bower_components/angular-route',
                        src: ['*.js'],
                        dest: 'public/lib/angular-route',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/slimScroll',
                        src: ['*.js'],
                        dest: 'public/lib/slimScroll',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-slimscroll',
                        src: ['*.js'],
                        dest: 'public/lib/angular-slimscroll',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-smart-table',
                        src: ['dist/*.js'],
                        dest: 'public/lib/angular-smart-table',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-toastr',
                        src: ['dist/*.js', 'dist/*.css'],
                        dest: 'public/lib/angular-toastr',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-touch',
                        src: ['*.js'],
                        dest: 'public/lib/angular-touch',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-ui-sortable',
                        src: ['*.js'],
                        dest: 'public/lib/angular-ui-sortable',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap',
                        src: ['**/*.js', '**/*.css', '**/*.map'],
                        dest: 'public/lib/bootstrap',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap-select',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/bootstrap-select',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap-switch',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/bootstrap-switch',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/bootstrap-tagsinput',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/bootstrap-tagsinput',
                        expand: true
                    },
                    { cwd: 'bower_components/moment', src: ['**/*.js'], dest: 'public/lib/moment', expand: true },
                    {
                        cwd: 'bower_components/fullcalendar',
                        src: ['dist/**/*.js', '**/*.css'],
                        dest: 'public/lib/fullcalendar',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/leaflet',
                        src: ['dist/*.js', '**/*.css'],
                        dest: 'public/lib/leaflet',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-progress-button-styles',
                        src: ['dist/**/*.js', '**/*.css'],
                        dest: 'public/lib/angular-progress-button-styles',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-ui-router',
                        src: ['**/*.js'],
                        dest: 'public/lib/angular-ui-router',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-chart.js',
                        src: ['**/*.js'],
                        dest: 'public/lib/angular-chart.js',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/chartist',
                        src: ['**/*.js', '**/*.css', '**/*.map'],
                        dest: 'public/lib/chartist',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-chartist.js',
                        src: ['**/*.js'],
                        dest: 'public/lib/angular-chartist.js',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/eve-raphael',
                        src: ['**/*.js'],
                        dest: 'public/lib/eve-raphael',
                        expand: true
                    },
                    { cwd: 'bower_components/raphael', src: ['**/*.js'], dest: 'public/lib/raphael', expand: true },
                    { cwd: 'bower_components/mocha', src: ['**/*.js'], dest: 'public/lib/mocha', expand: true },
                    {
                        cwd: 'bower_components/morris.js',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/morris.js',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-morris-chart',
                        src: ['**/*.js'],
                        dest: 'public/lib/angular-morris-chart',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/ionrangeslider',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/ionrangeslider',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-bootstrap',
                        src: ['**/*.js'],
                        dest: 'public/lib/angular-bootstrap',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-animate',
                        src: ['**/*.js'],
                        dest: 'public/lib/angular-animate',
                        expand: true
                    },
                    { cwd: 'bower_components/rangy', src: ['**/*.js'], dest: 'public/lib/rangy', expand: true },
                    {
                        cwd: 'bower_components/textAngular',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/textAngular',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-xeditable',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/angular-xeditable',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/jstree',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/jstree',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/ng-js-tree',
                        src: ['**/*.js'],
                        dest: 'public/lib/ng-js-tree',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-ui-select',
                        src: ['**/*.js', '**/*.css'],
                        dest: 'public/lib/angular-ui-select',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/animate.css',
                        src: ['*.css'],
                        dest: 'public/lib/animate.css',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/amcharts',
                        src: ['dist/amcharts/*.js', 'dist/amcharts/plugins/responsive/*.js', 'dist/amcharts/*.map', 'dist/amcharts/plugins/responsive/*.map'],
                        dest: 'public/lib/amcharts',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/ammap',
                        src: ['dist/ammap/*.js', 'dist/ammap/maps/**/*.js'],
                        dest: 'public/lib/ammap',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-local-storage',
                        src: ['dist/*.min.js', 'dist/*.map'],
                        dest: 'public/lib/angular-local-storage',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-resource',
                        src: ['*.min.js'],
                        dest: 'public/lib/angular-resource',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-spinner/dist/',
                        src: ['*.min.js'],
                        dest: 'public/lib/angular-spinner',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-material/',
                        src: ['*.js', '*.css'],
                        dest: 'public/lib/angular-material',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-aria/',
                        src: ['*.js'],
                        dest: 'public/lib/angular-aria',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/angular-google-maps/dist',
                        src: ['*.js'],
                        dest: 'public/lib/angular-google-maps',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/ngInfiniteScroll/build',
                        src: ['*.js'],
                        dest: 'public/lib/ng-infinite-scroll',
                        expand: true
                    },
                    //{cwd: 'bower_components/', src: ['**/*.css'], dest: 'public/lib/', expand: true},
                    //{cwd: 'bower_components/', src: ['**/*.js'], dest: 'public/lib/', expand: true},


                ]
            }
        },
        nodemon: {
            dev: {
                script: 'app.js',
                options: {
                    args: ['--color'],
                    ignore: ['README.md', 'node_modules/**', '.DS_Store'],
                    ext: 'js',
                    watch: ['app', 'config', 'app.js', 'gruntfile.js'],
                    delay: 1000,
                    env: {
                        PORT: 3000
                    },
                    cwd: __dirname
                }
            }
        },
        concurrent: {
            tasks: ['nodemon', 'watch'],
            options: {
                logConcurrentOutput: true
            }
        },
        mochaTest: {
            options: {
                reporter: 'spec'
            },
            src: ['test/mocha/**/*.js']
        },
        env: {
            test: {
                NODE_ENV: 'test'
            }
        },
        concat: {
            options: {
                separator: ';\n'
            },
            distdesk: {
                src: [
                    // JavaScript Files
                    'public/lib/jquery/dist/jquery.js',
                    'public/lib/jquery-ui/jquery-ui.js',
                    // Angular Modules
                    'public/lib/angular/angular.js',
                    'public/lib/angular-route/angular-route.js',
                    'public/lib/slimScroll/jquery.slimscroll.js',
                    'public/lib/angular-slimscroll/angular-slimscroll.js',
                    'public/lib/angular-smart-table/dist/smart-table.js',
                    'public/lib/angular-toastr/dist/angular-toastr.tpls.js',
                    'public/lib/angular-touch/angular-touch.js',
                    'public/lib/angular-ui-sortable/sortable.js',
                    'public/lib/moment/moment.js',
                    'public/lib/angular-progress-button-styles/dist/angular-progress-button-styles.min.js',
                    'public/lib/angular-ui-router/release/angular-ui-router.js',
                    'public/lib/ionrangeslider/js/ion.rangeSlider.js',
                    'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                    // 'public/lib/angular-animate/angular-animate.js',
                    'public/lib/angular-local-storage/dist/angular-local-storage.min.js',
                    'public/lib/angular-spinner/angular-spinner.min.js',
                    // 'public/lib/angular-aria/angular-aria.js',
                    'public/lib/angular-material/angular-material.js',
                    'public/lib/ng-infinite-scroll/ng-infinite-scroll.min.js',

                    // Application InIt
                    'public/app/common/config.js',
                    'public/app/pages/pages.module.js',
                    'public/app/theme/theme.module.js',
                    'public/app/common/apiService.js',
                    'public/app/common/common.services.js',

                    // Application Module Files
                    'public/app/theme/components/components.module.js',
                    'public/app/theme/inputs/inputs.module.js',
                    'public/app/app.js',
                    'public/app/theme/theme.config.js',
                    'public/app/theme/theme.configProvider.js',
                    'public/app/theme/theme.constants.js',
                    'public/app/theme/theme.run.js',
                    'public/app/theme/theme.service.js',
                    'public/app/theme/components/toastrLibConfig.js',
                    // 'public/app/theme/directives/animatedChange.js',
                    'public/app/theme/directives/autoExpand.js',
                    'public/app/theme/directives/autoFocus.js',
                    'public/app/theme/directives/includeWithScope.js',
                    'public/app/theme/directives/ionSlider.js',
                    'public/app/theme/directives/ngFileSelect.js',
                    'public/app/theme/directives/scrollPosition.js',
                    'public/app/theme/directives/trackWidth.js',
                    'public/app/theme/directives/zoomIn.js',
                    'public/app/theme/services/baProgressModal.js',
                    'public/app/theme/services/baUtil.js',
                    'public/app/theme/services/fileReader.js',
                    'public/app/theme/services/preloader.js',
                    'public/app/theme/services/stopableInterval.js',
                    'public/app/theme/components/backTop/backTop.directive.js',
                    'public/app/theme/components/baPanel/baPanel.directive.js',
                    'public/app/theme/components/baPanel/baPanel.service.js',
                    'public/app/theme/components/baPanel/baPanelBlur.directive.js',
                    'public/app/theme/components/baPanel/baPanelBlurHelper.service.js',
                    'public/app/theme/components/baPanel/baPanelSelf.directive.js',
                    'public/app/theme/components/baSidebar/baSidebar.directive.js',
                    'public/app/theme/components/baSidebar/baSidebar.service.js',
                    'public/app/theme/components/baSidebar/BaSidebarCtrl.js',
                    'public/app/theme/components/baSidebar/baSidebarHelpers.directive.js',
                    'public/app/theme/components/baWizard/baWizard.directive.js',
                    'public/app/theme/components/baWizard/baWizardCtrl.js',
                    'public/app/theme/components/baWizard/baWizardStep.directive.js',
                    'public/app/theme/components/contentTop/contentTop.directive.js',
                    'public/app/theme/components/msgCenter/msgCenter.directive.js',
                    'public/app/theme/components/msgCenter/MsgCenterCtrl.js',
                    'public/app/theme/components/pageTop/pageTop.directive.js',
                    'public/app/theme/components/progressBarRound/progressBarRound.directive.js',
                    'public/app/theme/components/widgets/widgets.directive.js',
                    'public/app/theme/filters/text/removeHtml.js',
                    'public/app/theme/filters/image/appImage.js',
                    'public/app/theme/filters/image/kameleonImg.js',
                    'public/app/theme/filters/image/profilePicture.js',
                    'public/app/theme/inputs/baSwitcher/baSwitcher.js',
                    'public/app/theme/components/backTop/lib/jquery.backTop.min.js',


                    // Add Modules Files here for Directive, Components, Controller, Services and Filters
                    'public/app/pages/login/login.module.js',
                    'public/app/pages/login/login.services.js',
                    'public/app/pages/login/login.controller.js',

                    'public/app/pages/dashboard/dashboard.module.js',
                    'public/app/pages/dashboard/dashboard.controller.js',
                    'public/app/pages/dashboard/dashboard.services.js',

                    // Rider Module files
                    'public/app/pages/sp/sp.module.js',
                    'public/app/pages/sp/sp.controller.js',
                    'public/app/pages/sp/spListing/spListing.controller.js',
                    'public/app/pages/sp/sp.services.js',

                    // Merchant Module files
                    'public/app/pages/merchantsAdmin/merchants.module.js',
                    'public/app/pages/merchantsAdmin/merchants.controller.js',
                    'public/app/pages/merchantsAdmin/merchantsListing/merchantListing.controller.js',
                    'public/app/pages/merchantsAdmin/merchants.services.js',

                    // user Module files
                    'public/app/pages/user/user.module.js',
                    'public/app/pages/user/user.controller.js',
                    'public/app/pages/user/user.services.js',

                    // Configuration Module files
                    'public/app/pages/configuration/configuration.module.js',
                    'public/app/pages/configuration/configuration.controller.js',
                    'public/app/pages/configuration/configuration.services.js',

                    // MerchantTypes Module files
                    'public/app/pages/merchantTypes/merchantTypes.module.js',
                    'public/app/pages/merchantTypes/merchantTypes.controller.js',
                    'public/app/pages/merchantTypes/merchantTypes.services.js',

                    // Merchant Notifications Service files
                    'public/app/pages/notifications/notifications.module.js',
                    'public/app/pages/notifications/notifications.services.js',

                    // Orders Module files
                    'public/app/pages/orders/orders.module.js',
                    'public/app/pages/orders/orders.controller.js',
                    'public/app/pages/orders/ordersListing/orderListing.controller.js',
                    'public/app/pages/orders/orders.services.js',

                    // payouts Module files
                    'public/app/pages/payouts/payouts.module.js',
                    'public/app/pages/payouts/payouts.controller.js',
                    'public/app/pages/payouts/payoutListing/payoutListing.controller.js',
                    'public/app/pages/payouts/payouts.services.js',

                    'public/app/pages/index.controller.js',

                    // Change Password Module files
                    'public/app/pages/changePassword/changePassword.module.js',
                    'public/app/pages/changePassword/changePassword.services.js',
                    'public/app/pages/changePassword/changePasswordListing/adminChangePassword.controller.js',

                    // Geo Fencing
                    'public/app/pages/cities/city.module.js',
                    'public/app/pages/cities/city.services.js',
                    'public/app/pages/cities/city.controller.js',

                    // Bank Module files
                    // 'public/app/pages/bank/bank.module.js',
                    // 'public/app/pages/bank/bank.controller.js',
                    // 'public/app/pages/bank/bank.services.js',

                    // Admin Promotion Module files
                    'public/app/pages/promotion/promotion.module.js',
                    'public/app/pages/promotion/promotion.services.js',
                    'public/app/pages/promotion/promotion.controller.js',

                    // Admin promotionCode Module files
                    'public/app/pages/promotionCode/promotion.module.js',
                    'public/app/pages/promotionCode/promotion.services.js',
                    'public/app/pages/promotionCode/promotion.controller.js',

                    // Admin promotionModule2 Module files
                    // 'public/app/pages/promotion2/promotion2.module.js',
                    // 'public/app/pages/promotion2/promotion2.services.js',
                    // 'public/app/pages/promotion2/promotion2.controller.js',

                ],
                dest: 'public/concat.js'
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            my_target_2: {
                files: {
                    'public/concat.min.js': ['public/concat.js']
                }
            }
        },
        concat_css: {
            options: {
                // Task-specific options go here.
            },
            all: {
                src: [
                    'public/js/bootstrap/dist/css/bootstrap.css',
                    // link(rel='stylesheet', href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900&subset=latin,greek,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic')
                    // 'public/assets/Ionicons/css/ionicons.css',
                    'public/lib/angular-toastr/dist/angular-toastr.css',
                    // 'public/lib/animate.css/animate.css',
                    'public/lib/bootstrap/dist/css/bootstrap.css',
                    'public/lib/bootstrap-select/dist/css/bootstrap-select.css',
                    'public/lib/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',
                    'public/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                    // 'public/assets/font-awesome/css/font-awesome.css',
                    'public/lib/fullcalendar/dist/fullcalendar.css',
                    'public/lib/leaflet/dist/leaflet.css',
                    'public/lib/angular-progress-button-styles/dist/angular-progress-button-styles.min.css',
                    'public/lib/chartist/dist/chartist.min.css',
                    'public/lib/morris.js/morris.css',
                    'public/lib/ionrangeslider/css/ion.rangeSlider.css',
                    'public/lib/ionrangeslider/css/ion.rangeSlider.skinFlat.css',
                    'public/lib/textAngular/dist/textAngular.css',
                    'public/lib/angular-xeditable/dist/css/xeditable.css',
                    'public/lib/jstree/dist/themes/default/style.css',
                    'public/lib/angular-ui-select/dist/select.css',
                    'public/lib/angular-material/angular-material.css',
                    // link(rel='stylesheet', href='app/main1.css')
                    'public/app/main.css',
                    'public/app/stylesheet.css',
                ],
                dest: 'public/dest/concat-style.css'
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'public/dest/concat-style.min.css': ['public/dest/concat-style.css']
                }
            }
        },
        'cache-busting': {
            requirejs: {
                replace: ['app/views/includes/foot.jade'],
                replacement: 'concat.js',
                file: 'public/concat.js',
                get_param: true
            },
            css: {
                replace: ['app/views/includes/head.jade'],
                replacement: 'concat-style.min.css',
                file: 'public/dest/concat-style.min.css',
                get_param: true
            }
        },
        karma: {
            unit: {
                configFile: 'test/karma/karma.conf.js'
            }
        }
    });

    // Load NPM tasks
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-nodemon');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-copy');
    grunt.loadNpmTasks('grunt-execute');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-cache-busting');

    // Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    // Default task(s).
    grunt.registerTask('default', ['copy', 'jshint', 'concurrent']);

    // Test task.
    grunt.registerTask('test', ['env:test', 'mochaTest', 'karma:unit']);

    // concats
    grunt.registerTask('autorun', ['concat:distdesk']);

    grunt.registerTask('build', ['concat:distdesk', 'uglify:my_target_2', 'concat_css', 'cssmin', 'cache-busting']);
};