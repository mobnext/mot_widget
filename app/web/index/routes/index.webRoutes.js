/*
 Programmer: Muhammad Raza Tayyab
 Date: April 15, 2019
 */

const accountsController = require('../controllers/index.controller');

module.exports = (app, webVersion) => {
    app.get(webVersion + '/index',
        accountsController.index
    );

    app.get(webVersion + '/search/result',
        accountsController.searchResult
    );
};