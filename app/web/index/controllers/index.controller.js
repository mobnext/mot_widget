const winston = require('winston'),
    request = require('request'),
    axios = require("axios"),
    moment = require('moment');

// index
let index = async (req, res, next) => {
    let data = {message: '', regNumber: '', postCode: ''};
    res.render('home/index', data);
};

let searchResult = async (req, res, next) => {

    let regNumber = req.query.registrationNo,
        postCode = req.query.postCode;

    if (!regNumber || !postCode) {
        res.redirect('home/index');
    }

    let data = {
        categoryDescriptions: {
            brakes: 'Possible fault relating to one or more of Condition and operation, Service brake pedal or hand lever pivot, Service brake pedal or hand lever condition and travel, Air and vacuum systems, Low pressure warning, Hand operated brake control valve, Parking brake lever or control, Brake valves, Pressure storage reservoirs, Brake servo units and master cylinder (hydraulic systems),Rigid brake pipes, Flexible brake hoses, Brake linings and pads, Brake discs and drums, Brake cables, rods, levers and linkages, Brake actuators – including spring brakes, hydraulic cylinders and callipers, Load sensing valve, Brake slack adjuster, Endurance braking system (where fitted), Complete braking system, Service brake performance and efficiency, Secondary brake performance and efficiency Parking brake performance and efficiency, Additional braking device (retarder) performance, Anti-lock braking system (ABS), Electronic braking system (EBS), Brake fluid...',
            steering: 'Possible fault relating to one or more of Mechanical condition (Steering gear condition, Steering gear security, Steering linkage condition, Steering linkage operation, Power steering) Steering wheel and column or handlebar, forks and yokes, Steering play, Electronic power steering...',
            body: 'Possible fault relating to Structure and attachments,(General condition Exhaust system, Fuel system, Bumpers, Spare wheel carrier (if fitted), Coupling mechanisms and towing equipment, Transmission, Engine mountings), Body and interior (Body condition, Cab and body mounting, Doors and door catches, Floor, Driver’s seat, Passenger seats, Driving controls, Cab steps (if fitted), Handgrips and footrests)...',
            lamps: `Possible fault relating to one or more of (Presence, condition and operation Switching, Compliance with requirements), Headlamps ,Headlamp alignment, Levelling devices, Headlamp cleaning devices, Front and rear position lamps, side marker lamps and end-outline marker lamps, Stop lamps, Direction indicators and hazard warning lamps, Flashing frequency, Front and rear fog lamps, Reversing lamps, Rear registration plate lamps, Rear reflectors ‘Tell-tales’ mandatory for lighting equipment, Trailer electrical socket, Electrical wiring, Battery...`,
            visibility: 'Possible fault relating to one or more of Field of vision, Condition of glass, View to rear, Windscreen wipers, Windscreen washers...',
            nuisance: 'Possible fault relating to one or more of Noise suppression system, Exhaust emissions (Spark ignition engine emissions, Exhaust emission control equipment, Gaseous emissions, Compression ignition engine emissions, Exhaust emission control equipment, Opacity), Fluid leak...',
            wheels: 'Possible fault relating to Axles, Stub axles, Wheel bearings, Wheels and tyres, Road wheel and hub, Road wheel condition, Tyres, Suspension, Springs, Shock absorbers, Suspension arms, rods, struts, sub-frames, anti-roll bars etc. Suspension joints, pins and bushes, Gas, air and fluid suspension, Complete suspension system...'
        },
    };
    let errorMesage = '';
    try {
        errorMesage = 'Registration number not found';
        const predictionResponse = await axios({
            method: 'get',
            url: 'https://api.vmot.co.uk/predictions/' + regNumber,
            headers: {
                'content-type': 'application/json; charset=utf-8',
                'x-api-key': 'dAgvrAtEaXZ6BSXaFcdWtmk4cGYJLy4c'
            },
        });

        console.log(predictionResponse)
        let predictionData = predictionResponse.data;
        if (predictionData && predictionData.error) {
            let data = {
                message: errorMesage,
                regNumber: regNumber,
                postCode: postCode
            }
            return res.render('home/index', data);
        } else {
            data.prediction = predictionData;
            data.failProbability = predictionData.fail_probability * 100;
        }

        errorMesage = 'Invalid Post Code';
        const postCodeResponse = await axios({
            method: 'post',
            url: 'https://api.vmot.co.uk/v-api/postcode/' + postCode,
            headers: {
                'content-type': 'application/json; charset=utf-8',
                'w-api-key': 'dAgvrAtEaXZ6BSXaFcdWtmk4cGYJLy4c',
                'Access-Control-Allow-Origin': '*',
            },
        });

        let postCodeData = postCodeResponse.data;
        if (postCodeData && postCodeData.error) {
            let data = {
                message: errorMesage,
                regNumber: regNumber,
                postCode: postCode
            }
            return res.render('home/index', data);
        } else {
            data.postCodeData = postCodeData;
        }

        const compainsResponse = await axios({
            method: 'post',
            url: 'https://api.vmot.co.uk/v-api/campaigns',
            headers: {
                'content-type': 'application/json; charset=utf-8',
                'w-api-key': 'dAgvrAtEaXZ6BSXaFcdWtmk4cGYJLy4c',
                'Access-Control-Allow-Origin': '*',
            },
            data: {
                registration_number: regNumber,
                post_code: postCode
            },
            responseType: 'json',
        });
        let compainsData = compainsResponse.data;
        if (compainsData) {

            if (compainsData.error) {
                errorMesage = compainsData.error;
                let data = {
                    message: errorMesage,
                    regNumber: regNumber,
                    postCode: postCode
                }
                return res.render('home/index', data);
            }
            data.prediction = compainsData.prediction;
            data.campaigns = compainsData.campaigns;
            data.vouchers = compainsData.vouchers;
            data.prediction_by_categories = compainsData.prediction_by_categories;
            customerId = compainsData.customer_id;
            const l1 = 33;
            const l2 = 66;
            data.customerId = compainsData.customer_id;
            data.categoryClasses = {
                brakes: data.prediction_by_categories.brakes < l1 ? 'info-success' : data.prediction_by_categories.brakes < l2 ? 'info-warning' : 'info-danger',
                steering: data.prediction_by_categories.steering < l1 ? 'info-success' : data.prediction_by_categories.steering < l2 ? 'info-warning' : 'info-danger',
                body: data.prediction_by_categories.body < l1 ? 'info-success' : data.prediction_by_categories.body < l2 ? 'info-warning' : 'info-danger',
                lamps: data.prediction_by_categories.lamps < l1 ? 'info-success' : data.prediction_by_categories.lamps < l2 ? 'info-warning' : 'info-danger',
                visibility: data.prediction_by_categories.visibility < l1 ? 'info-success' : data.prediction_by_categories.visibility < l2 ? 'info-warning' : 'info-danger',
                nuisance: data.prediction_by_categories.nuisance < l1 ? 'info-success' : data.prediction_by_categories.nuisance < l2 ? 'info-warning' : 'info-danger',
                wheels: data.prediction_by_categories.wheels < l1 ? 'info-success' : data.prediction_by_categories.wheels < l2 ? 'info-warning' : 'info-danger',
                other: data.prediction_by_categories.other < l1 ? 'info-success' : data.prediction_by_categories.other < l2 ? 'info-warning' : 'info-danger',
            }

            for (let i = 0; i < data.vouchers.data.length; i++) {
                let voucherData = data.vouchers.data[i];
            }

            data.moment = moment;
            data.border = data.failProbability < l1 ? 'border-success' : data.failProbability < l2 ? 'border-warning' : 'border-danger';
            data.textColor = data.failProbability < l1 ? 'text-success' : data.failProbability < l2 ? 'text-warning' : 'text-danger';
            res.render('home/searchResult', data);
        }
    } catch (err) {
        winston.error((err.message) ? err.message : err);
        let data = {
            message: errorMesage,
            moment,
            regNumber: regNumber,
            postCode: postCode
        }
        return res.render('home/index', data);
    }
}

let coupon = async (req, res, next) => {
    res.render('home/index');

}

module.exports = {
    index,
    searchResult,
    coupon
};
