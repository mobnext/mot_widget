'use strict';
const glob = require('glob'),
    winston = require('winston');

module.exports = (app) => {
    let webUserRoutes = 'app/web/**/*.webRoutes.js';

    let webVersion = '';
    glob.sync(webUserRoutes).forEach((file) => {
        require('../' + file)(app, webVersion);
        winston.info(file + ' is loaded');
    });
};