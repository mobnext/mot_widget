var glob = require('glob'),
    path = require('path'),
    env = process.env.NODE_ENV || 'development',
    async = require('async');

global.config = {};

module.exports = callback => {

    async.series([
        envCb => {
            // configuring the environment
            glob('config/env/**/*.json', (err, files) => {

                if (err) {
                    return envCb(err);
                } else {
                    // picking up the environment file
                    config = require(path.join(__dirname, 'env', env + '.json'));
                    if (!config) {
                        return envCb('error occured while loading config file!');
                    } else {
                        envCb();
                    }
                }
            });

        }
    ], err => {
        if (err) {
            return callback(err);
        } else {
            return callback();
        }
    });
};