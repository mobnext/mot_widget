/**
 * Created by Asif on 7/5/2017.
 */

"use strict"; // NOSONAR
const errors = require('./errors');

let successResponse = (res, data) => {
    data.response = 200;
    res.json(data);
};

let errorResponse = (res, err) => {

    res.json({
        success: 0,
        data: {},
        message: err,
        response: 304
    });

};

let errorResponseWithData = (req, res, responseCode, successCode, err, data) => {
    res.status(200);
    res.json({
        success: successCode,
        message: errors[err.msgCode].msg.EN,
        response: responseCode,
        data: data
    });
};

module.exports = {
    successResponse,
    errorResponse,
    errorResponseWithData
};
