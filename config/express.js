const express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    flash = require('connect-flash'),
    http = require('http'),
    chalk = require('chalk'),
    winston = require('winston');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, '../app/views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./config')(err => {
    if (err) {
        winston.error(err);
    } else {
        // Normalize a port into a number, string, or false.
        function normalizePort(val) {
            var port = parseInt(val, 10);

            if (isNaN(port)) {
                // named pipe
                return val;
            }

            if (port >= 0) {
                // port number
                return port;
            }

            return false;
        }

        /**
         * Event listener for HTTP server "error" event.
         */

        function onError(error) {
            if (error.syscall !== 'listen') {
                throw error;
            }

            var bind = typeof port === 'string' ?
                'Pipe ' + port :
                'Port ' + port;

            // handle specific listen errors with friendly messages
            switch (error.code) {
                case 'EACCES':
                    console.error(bind + ' requires elevated privileges');
                    process.exit(1);
                    break;
                case 'EADDRINUSE':
                    console.error(bind + ' is already in use');
                    process.exit(1);
                    break;
                default:
                    throw error;
            }
        }

        /**
         * Event listener for HTTP server "listening" event.
         */

        function onListening() {
            var addr = server.address();
            var bind = typeof addr === 'string' ?
                'pipe ' + addr :
                'port ' + addr.port;
            console.log(chalk.bold.green('Server is listening on', bind));
        }

        // Create HTTP server.
        var server = http.createServer(app);

        /**
         * Get port from environment and store in Express.
         */
        var port = normalizePort(config.PORT || '3000');

        server.listen(port);
        server.on('error', onError);
        server.on('listening', onListening);

        app.use(cookieParser());
        app.use(express.static(path.join(__dirname, '../public')));
        app.use(flash());
        const errors = require('./errors');
        require('./routes')(app);

        app.get('/', (req, res, next) => {
            return res.redirect('/index');
        });
        app.post('/coupon', (req, res, next) => {
            console.log("copuon request received ---------------------------"+req)
            return res.redirect('/coupon');
            // return res.redirect('/coupon');
        });

        // catch 404 and forward to error handler
        app.use((req, res, next) => {
            var err = new Error('Not Found');
            err.status = 404;
            next(err);
        });

        // error handlers
        app.use((err, req, res, next) => {
            res.status(err.status || 500);
            if (err.status !== 404) {
                winston.error(err);
            }

            if (err.message && typeof err.message === 'number') {
                err.msgCode = err.message;
            }

            if (err.name == 'ValidationError') {
                res.json({
                    success: 0,
                    message: err.errors,
                    response: 200,
                    data: {}
                });
            } else {
                if (err.msgCode == '0003') {
                    res.status(401);
                    err.status = 401;
                }
                if (err.msgCode == '0004') {
                    res.status(403);
                }

                if (err.status == 404) {
                    res.json({
                        success: 0,
                        message: 'Not Found.',
                        response: 404,
                        data: {}
                    });
                } else if (err.status == 401) {
                    res.json({
                        success: 0,
                        message: errors[err.msgCode].msg.EN,
                        response: 400,
                        data: {}
                    });
                } else {
                    if (!err.msgCode) {
                        res.status(500);
                        res.json({
                            success: 0,
                            message: 'Something went wrong. Please try again.',
                            response: 500,
                            data: {}
                        });
                    } else {
                        res.status(200);
                        res.json({
                            success: 0,
                            message: errors[err.msgCode].msg.EN,
                            response: 400,
                            data: {}
                        });
                    }
                }
            }
        });

    }
});

module.exports = app;